we're enhancing your CI/CD pipeline with an automated deployment step to an EC2 instance using Jenkins. This involves adding an SSH key for secure access, crafting a shell script for deployment commands, and integrating these components into your Jenkinsfile. Here's how to set it up:

### 1. Prepare Jenkins for SSH Access
Within Jenkins, create SSH credentials named ec2-server-key, incorporating the private SSH key content from WebServerKeyPair.pem. This step ensures Jenkins can securely SSH into your EC2 server.

### 2. Create a Deployment Script (server-cmds.sh)
This script will be executed on your EC2 instance to deploy your Dockerized application using Docker Compose.

```bash
#!/bin/bash
export IMAGE=$1
docker-compose -f docker-compose.yaml up --detach
echo "success"
```

Make sure to set executable permissions on this script (chmod +x server-cmds.sh) before committing it to your repository.

### 3. Update the Jenkinsfile
Incorporate the 'deploy to EC2' stage in your pipeline, leveraging the sshagent plugin for Jenkins to handle SSH key management and command execution on the EC2 instance.

```bash
pipeline {
    agent any
    tools {
        nodejs "node"
    }
    stages {
        stage('increment version') {
            // Your version increment logic
        }
        stage('Run tests') {
            // Your testing logic
        }
        stage('Build and Push docker image') {
            // Your Docker build and push logic
        }
        stage('deploy to EC2') {
            steps {
                script {
                    def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
                    def ec2Instance = "ec2-user@{public-ip-address}"

                    sshagent(['ec2-server-key']) {
                        sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
                        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
                    }
                }
            }
        }
        stage('commit version update') {
            // Your version commit logic
        }
    }
}
```

Replace {public-ip-address} with the actual IP address of your EC2 instance and ${IMAGE_NAME} with the Docker image name variable used in your pipeline.

### 4. Best Practices and Considerations
- Security: Ensure the security group for your EC2 instance allows SSH access only from your Jenkins server's IP address.
- Docker Compose Versioning: If your application relies on specific versions of Docker or Docker Compose, ensure those versions are installed on your EC2 instance.
- Pipeline Efficiency: Consider adding conditions to your deployment stage to run only on specific branches (e.g., main/master) or when specific criteria are met.

This setup automates the deployment process, significantly reducing manual intervention and ensuring that your application is always up-to-date with the latest changes committed to your repository.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/aws-services/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
