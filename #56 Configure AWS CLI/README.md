# Exercise 2: Configure AWS CLI

Using the AWS Command Line Interface (CLI) is essential for managing AWS services efficiently. This exercise guides you through setting up the AWS CLI with new user credentials and configuring the default region, ensuring seamless interaction with your AWS account.

## Prerequisites

- AWS CLI installed on your machine.
- Admin user access to obtain `new-access-key-id` and `new-secret-access-key`.
- The `key.txt` file with the new user credentials.

## Configuration Steps

### Step 1: Save Current Admin User Keys

It's crucial to back up existing credentials before making any changes:

1. **Locate the AWS credentials file**: Typically found at `~/.aws/credentials` on Linux and macOS, or `%UserProfile%\.aws\credentials` on Windows.
2. **Backup the credentials**: Securely copy and save the contents of the `credentials` file to a safe location.

### Step 2: Set Credentials for the New User

To update the AWS CLI with new user credentials, follow these steps:

1. Open your terminal or command prompt.
2. Run the `aws configure` command.
3. Enter the new user's credentials and configuration details when prompted:
   - **AWS Access Key ID [None]:** `new-access-key-id` (from your `key.txt` file)
   - **AWS Secret Access Key [None]:** `new-secret-access-key` (from your `key.txt` file)
   - **Default region name [None]:** `new-region` (desired AWS region, e.g., `us-east-1`)
   - **Default output format [None]:** (Optional, press Enter to skip)

```shell
$ aws configure
AWS Access Key ID [None]: new-access-key-id
AWS Secret Access Key [None]: new-secret-access-key
Default region name [None]: new-region
Default output format [None]: 
```

### Step 3: Validate Configuration
Ensure the new credentials are correctly set:

- Verify the ~/.aws/credentials file contains the new user's keys.
- Test the AWS CLI configuration by listing AWS S3 buckets, assuming the new user has the necessary permissions:

```bash
aws s3 ls
```

A successful command execution indicates proper AWS CLI configuration.

### Best Practices
- Keep AWS access keys confidential and secure.
- Utilize AWS IAM roles and policies for secure access control.
- Regularly review and rotate AWS credentials for enhanced security.

This guide outlines the process for configuring the AWS CLI with new user credentials and setting a default region, essential for effective AWS service management from the command line.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/aws-services/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
