# EXERCISE 1: Create IAM User and Set Permissions

In this exercise, you will create an IAM user with the necessary permissions to perform AWS tasks. Follow these steps:

1. Check if you have already configured an AWS admin user locally using the AWS CLI. You can verify it with the following commands:
   
```bash
aws configure list
cat ~/.aws/credentials
```

Create a new IAM user using your name (e.g., "nana") with both UI and CLI access:

```bash
aws iam create-user --user-name nana
```

Create an IAM group named "devops":

```bash
aws iam create-group --group-name devops
```

Add the user you created in step 2 to the "devops" group:

```bash
aws iam add-user-to-group --user-name nana --group-name devops
```

Verify that the "devops" group contains your user:

```bash
aws iam get-group --group-name devops
```

Generate access keys for CLI access and save them securely:

```bash
aws iam create-access-key --user-name nana > key.txt
```

Make sure to store the access keys in a safe location.

Generate login credentials for UI access and save the password securely:
```bash
aws iam create-login-profile --user-name nana --password MyTestPassword123
```

Ensure that you store the password in a secure manner.

Give the user permission to change their password:

```bash
aws iam list-policies | grep ChangePassword
aws iam attach-user-policy --user-name nana --policy-arn "arn:aws:iam::aws:policy/IAMUserChangePassword"
```

Check which policies are available for managing EC2 and VPC services, including subnets and security groups:
```bash
aws iam list-policies | grep EC2FullAccess
aws iam list-policies | grep VPCFullAccess
```

Give the "devops" group the needed permissions for EC2 and VPC:

```bash
aws iam attach-group-policy --group-name devops --policy-arn "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
aws iam attach-group-policy --group-name devops --policy-arn "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
```

Check the policies attached to the "devops" group:
```bash
aws iam list-attached-group-policies --group-name devops
```

You have now created an IAM user with the necessary permissions and added them to a group for managing AWS services.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/aws-services/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
