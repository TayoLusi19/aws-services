# Exercise 4: Create EC2 Instance in VPC

This exercise guides you through creating an Amazon EC2 instance within your custom VPC, utilizing the security group for SSH access, and setting up a key pair for secure connections.

## Steps to Create an EC2 Instance

### Create a Key Pair

1. **Generate a new key pair** and save it locally. This key will be used to securely SSH into the EC2 instance:

```shell
aws ec2 create-key-pair --key-name WebServerKeyPair --query "KeyMaterial" --output text > WebServerKeyPair.pem
chmod 400 WebServerKeyPair.pem
```

This command creates a new file WebServerKeyPair.pem in your current directory. The chmod 400 command modifies the file permissions to ensure that it is readable by the owner only, which is a requirement for SSH key files.

### Launch an EC2 Instance
Create an EC2 instance within your subnet and security group. You will need to specify an AMI ID (image-id), which can be obtained from the AWS Management Console or AWS CLI by checking the latest available AMI for your region:

```bash
aws ec2 run-instances --image-id {image-id} --count 1 --instance-type t2.micro --key-name WebServerKeyPair --security-group-ids {sg-id} --subnet-id {subnet-id} --associate-public-ip-address
```

Replace {image-id}, {sg-id}, and {subnet-id} with the actual values for your configuration.

### Validate the EC2 Instance
Check the status of your EC2 instance and obtain its public IP address for SSH access. Replace instance-id with the actual instance ID returned from the previous command:

```bash
aws ec2 describe-instances --instance-id {instance-id} --query "Reservations[*].Instances[*].{State:State.Name,Address:PublicIpAddress}"
```

This command outputs the instance state and its public IP address, which you can use to connect via SSH.

### Best Practices
- Security: Use security groups to tightly control access to your EC2 instances. Limit SSH access to known IP addresses only.
- Resource Management: Regularly review and terminate unused instances to avoid unnecessary charges.
- Monitoring: Enable CloudWatch monitoring on your EC2 instance to track performance and set up alerts for unusual activity.
By following these steps, you will have successfully launched an EC2 instance within your custom VPC, ready for SSH access and further configuration for your applications.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/aws-services/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
