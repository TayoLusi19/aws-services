# Exercise 5: SSH into EC2 Instance and Install Docker

After successfully creating an EC2 instance, the next step is to prepare it for running Docker containers. This exercise covers accessing your instance via SSH and installing Docker on it.

## Steps to Access EC2 Instance and Install Docker

### SSH into Your EC2 Instance

1. **Connect to your EC2 instance** using SSH with the generated key pair. Ensure the EC2 instance is running and you have the correct public IP address:

```shell
ssh -i "WebServerKeyPair.pem" ec2-user@{public-ip-address}
```

Replace {public-ip-address} with the EC2 instance's public IP address obtained in the previous exercise. The path to WebServerKeyPair.pem should be relative or absolute to where the command is executed.

Install Docker on the EC2 Instance
Update the package repository and install Docker:

```shell
sudo yum update -y && sudo yum install -y docker
```

Start the Docker service to ensure Docker runs automatically with your instance:

```shell
sudo systemctl start docker
```

Add the ec2-user to the Docker group to allow running Docker commands without sudo:

```shell
sudo usermod -aG docker ec2-user
```

After executing these commands, you may need to log out and log back in for the group addition to take effect, enabling ec2-user to run Docker commands without sudo privileges.

### Post-Installation Steps
- Log out and log back in to refresh your session and apply the group changes.
- Verify Docker installation by running docker --version to ensure Docker CLI is working correctly.
- Test Docker with a simple container run, for example, docker run hello-world, to confirm everything is set up properly.
By completing these steps, your EC2 instance will be ready to host Docker containers, providing a flexible environment for deploying applications.

### Best Practices
- Security Updates: Regularly update your instance's software packages to ensure security vulnerabilities are patched.
- Docker Management: Familiarize yourself with Docker's command-line tools and best practices for managing containers effectively.
- Instance Monitoring: Utilize AWS CloudWatch for monitoring instance performance and Docker daemon logs to ensure smooth operation.

# Contributions
Contributions to this guide are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
LICENSE.md

# Author
Adetayo Michael Ibijemilusi






