# Exercise 3: Create VPC for EC2 Instance

Creating a dedicated Virtual Private Cloud (VPC) is essential for managing your AWS resources in an isolated network. This exercise walks you through the process of creating a new VPC with a subnet and setting up a security group that allows SSH and browser access to your Node application.

## Steps to Create a VPC with 1 Subnet

## Create a New VPC

1. **Create the VPC** and capture the VPC ID:

```shell
aws ec2 create-vpc --cidr-block 10.0.0.0/16 --query 'Vpc.VpcId' --output text
```

## Create a Subnet within the VPC
Create the subnet specifying the VPC ID:

```shell
aws ec2 create-subnet --vpc-id {vpc-id} --cidr-block 10.0.1.0/24
```

Retrieve the subnet ID by filtering with the VPC ID:

```shell
aws ec2 describe-subnets --filters "Name=vpc-id,Values={vpc-id}"
```

## Make the Subnet Public
Create an Internet Gateway and retrieve its ID:

```shell
aws ec2 create-internet-gateway --query 'InternetGateway.InternetGatewayId' --output text
```

Attach the Internet Gateway to your VPC:

```shell
aws ec2 attach-internet-gateway --vpc-id {vpc-id} --internet-gateway-id {igw-id}
```

Create a custom Route Table for your VPC and capture the Route Table ID:

```shell
aws ec2 create-route-table --vpc-id {vpc-id} --query 'RouteTable.RouteTableId' --output text
```

Create a Route in the Route Table for handling all traffic (0.0.0.0/0) through the Internet Gateway:

```shell
aws ec2 create-route --route-table-id {rtb-id} --destination-cidr-block 0.0.0.0/0 --gateway-id {igw-id}
```

Validate the Route Table configuration:

```shell
aws ec2 describe-route-tables --route-table-id {rtb-id}
```

Associate the Subnet with the Route Table to enable internet access:

```shell
aws ec2 associate-route-table --subnet-id {subnet-id} --route-table-id {rtb-id}
```

## Create a Security Group in the VPC
###Allow SSH Access on Port 22
Create a Security Group in your VPC and note the Security Group ID:

```shell
aws ec2 create-security-group --group-name SSHAccess --description "Security group for SSH access" --vpc-id {vpc-id}
```

Add a Rule to the Security Group to allow incoming SSH access (port 22) from any IP address:

```shell
aws ec2 authorize-security-group-ingress --group-id {sg-id} --protocol tcp --port 22 --cidr 0.0.0.0/0
```
For enhanced security, consider restricting SSH access to your IP address only by replacing 0.0.0.0/0 with your IP address CIDR block.

### Best Practices
- Security: Regularly review and update security group rules to enforce the principle of least privilege.
- Networking: Use multiple subnets and Availability Zones to improve the fault tolerance and availability of your applications.
- Monitoring: Enable CloudWatch Logs for network monitoring and VPC Flow Logs to capture information about the IP traffic going to and from network interfaces in your VPC.

This guide provides a detailed walkthrough for creating a VPC, subnet, and security group setup, facilitating a secure and isolated environment for your EC2 instance.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/aws-services/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi








